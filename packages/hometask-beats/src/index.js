import "./styles.css";

import createHeader from "./components/header/header";
import Hero from "./components/hero/hero";
import Collection from "./components/collection/collection";
import Features from "./components/features/features";


createHeader();

const hero = Hero.render();
const collection = Collection.render();
const features = await Features.render();

document.querySelector(".container").append(hero);
document.querySelector("#main").append(collection);
document.querySelector("#main").append(features);






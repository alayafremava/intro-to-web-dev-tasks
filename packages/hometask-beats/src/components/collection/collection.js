import "./collection.css"

const Collection = {
  render: () => {
    const collection = document.createElement("section");

    collection.classList.add("collection");
    collection.classList.add("section");

    collection.innerHTML = `
          <h2 class="section__title">
            Our Latest<br />
            colour collection 2021
          </h2>
          <div class="slider">
            <a class="slider__btn slider__prev"
              ><img src="./assets/icons/arrow-left.svg" alt="Left button"
            /></a>
            <img
              class="slider__img"
              src="./assets/images/clour-01.png"
              alt="He"
            />
            <img
              class="slider__img slider__img--active"
              src="./assets/images/clour-02.png"
            />
            <img class="slider__img" src="./assets/images/clour-03.png" />
            <a class="slider__btn slider__next"
              ><img src="./assets/icons/arrow-right.svg" alt="Right button"
            /></a>
          </div>
          `;

    return collection;
  },
};

export default Collection;

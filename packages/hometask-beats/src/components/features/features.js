import "./features.css";
import featuresData from "./features.json";

export class FeaturesService {
  #maxLimit = 10;

  /**
   *
   * @param {number} limit amount of features to load
   */
  getFeatures(limit = 3) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (limit > this.#maxLimit) {
          reject(new Error("Extended max limit"));
        }
        const features = featuresData.slice(0, limit);

        resolve(features);
      }, 100);
    });
  }
}

const Features = {
  render: async () => {
    const featuresService = new FeaturesService();

    const featuresContainer = document.createElement("section");

    featuresContainer.classList.add("features");
    featuresContainer.classList.add("section");

    const featuresItems = (await featuresService.getFeatures()).map(
      (feature) => `
      <li class="features__item">
      <div class="features__item__circle-outer">
        <div class="features__item__circle-inner">
          <img
            class="features__item__img"
            src=${feature.icon}
          />
        </div>
      </div>
      <div class="features__item__discription">
        <h3 class="features__item__title">${feature.name}</h3>
        <span class="features__item__text"
          >${feature.discription}</span
        >
        <a href="#" class="features__item__link">Lern More</a>
      </div>
    </li>
                `
    );
    
    featuresContainer.innerHTML = `
    <div class="features-left">
      <h2 class="section__title">
        Good headphones<br />
        and loud music is all<br />
        you need
      </h2>
      <ul class="features__list">
      ${featuresItems}
      </ul>
    </div>
    <div class="features-right">
      <img src="./assets/images/feature.png" alt="Headphones" />
    </div>
          `;

    return featuresContainer;
  },
};

export default Features;

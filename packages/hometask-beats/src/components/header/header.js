import "./header.css";

export default function createHeader() {
  const header = document.createElement("header");

  header.classList.add("header");

  header.innerHTML = `
  <a class="logo" href="#">
    <img src="./assets/icons/logo.svg" alt="Beats Logo" />
  </a>
  <nav>
    <ul class="nav-list">
      <li class="nav-item">
        <a class="nav-item__link nav-item__circle" href="#">
          <img
            class="nav-item__img"
            src="./assets/icons/search.svg"
            alt="Search navigation"
          />
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-item__link nav-item__circle" href="#">
          <img
            class="nav-item__img"
            src="./assets/icons/box.svg"
            alt="Box navigation"
          />
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-item__link nav-item__circle" href="#">
          <img
            class="nav-item__img"
            src="./assets/icons/user.svg"
            alt="User navigation"
          />
        </a>
      </li>
    </ul>
  </nav>

  <div class="burger-menu">
    <a class="burger-menu__link" href="#">
      <img
        class="burger-menu__img"
        src="./assets/icons/menu.svg"
        alt="Search navigation"
      />
    </a>
  </div>
      `;

  document.querySelector(".container").append(header);
}




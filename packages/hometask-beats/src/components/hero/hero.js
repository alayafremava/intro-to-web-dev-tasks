const Hero = {
  render: () => {
    const hero = document.createElement("section");

    hero.classList.add("hero");

    hero.innerHTML = `
    <div class="hero__img">
      <img src="./assets/images/hero.png" alt="Beats headphones" />
    </div>

    <div class="hero__description">
      <h4 class="hero__slogan">Hear it. Feel it</h4>
      <h1 class="hero__title">
        Move<br />
        with the<br />
        music
      </h1>

      <div class="price">
        <div class="price__new">$ 435</div>
        <div class="price__old">$ 465</div>
      </div>

      <button class="hero__cta">Buy now</button>
    </div>
          `;

    return hero;
  },
};

export default Hero;

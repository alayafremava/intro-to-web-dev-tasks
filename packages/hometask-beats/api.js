export const features = [
  {
    name: "Battery",
    discription: "Battery 6.2V-AAC codec",
    icon: "./assets/icons/battery.svg",
  },
  {
    name: "Bluetooth",
    discription: "Battery 6.2V-AAC codec",
    icon: "./assets/icons/bluetooth.svg",
  },
  {
    name: "Microphone",
    discription: "Battery 6.2V-AAC codec",
    icon: "./assets/icons/microphone.svg",
  },
];

export const products = [
  {
    bc: "#FFE5EE",
    icon: "./assets/icons/shopping-cart.svg",
    img: "./assets/images/product-03.png",
    name: "Read Headphone",
    price: "$ 256",
  },
  {
    bc: "#E5F1FF",
    icon: "./assets/icons/shopping-cart.svg",
    img: "./assets/images/product-02.png",
    name: "Blue Headphone",
    price: "$ 235",
  },
  {
    bc: "#E5FFFB",
    icon: "./assets/icons/shopping-cart.svg",
    img: "./assets/images/product-01.png",
    name: "Green Headphone",
    price: "$ 245",
  },
];
